package com.hello.world;

import com.hello.world.model.Utilisateur;
import com.hello.world.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@SpringBootApplication
@RestController
public class DemoApplication {

    @Autowired
    UserRepository usersRepository;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @GetMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        return String.format("Hello %s!", name);
    }

    @GetMapping("/users")
    public List<Utilisateur> getAllUsers(){
        return usersRepository.findAll();
    }

    @GetMapping("/save")
    public Utilisateur addUser(@RequestParam(value = "name") String name,@RequestParam(value = "pwd") String pwd,@RequestParam(value = "email") String email,@RequestParam(value = "phoneNumber") String phoneNumber){
        Utilisateur user = new Utilisateur(name,pwd,email,phoneNumber);
        return usersRepository.save(user);
    }

    @GetMapping("/user")
    public Utilisateur getUser(@RequestParam(value = "name") String name,@RequestParam(value = "pwd") String pwd){
        return usersRepository.findByUserAndPassword(name,pwd);
    }
}