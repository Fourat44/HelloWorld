package com.hello.world.repository;

import com.hello.world.model.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<Utilisateur,Long> {

   Utilisateur findByUserAndPassword(String user, String password);
}
